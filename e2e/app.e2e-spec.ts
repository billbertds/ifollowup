import { IfollowupPage } from './app.po';

describe('ifollowup App', function() {
  let page: IfollowupPage;

  beforeEach(() => {
    page = new IfollowupPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
