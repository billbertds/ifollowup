import { Component, OnInit } from '@angular/core';
import {FollowupService} from "../followup.service";
import {Router} from "@angular/router";
import {User} from "../user";
import {UserService} from "../user.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [FollowupService, UserService]
})
export class DashboardComponent implements OnInit {
  regions;
  selectedRegionNumber;
  followups;
  currentUser;
  dayOptions = [
    {labelName : "Past Due",
     dayValue : "0"},
    {labelName : "Today",
      dayValue : "1"},
    {labelName : "Next 3 Days",
      dayValue : "4"},
    {labelName : "Next 30 Days",
      dayValue : "31"},
    {labelName : "Next 60 Days",
      dayValue : "61"},
    {labelName : "All",
      dayValue : "101"}
  ];
  selectedDayOption = "0";
  followupUsers:User[] = [];
  selectedOwner:string;
  ownerRegionCriteria:string = "region";

  constructor(private followupService: FollowupService, private userService: UserService,
              private router: Router) {
    let user = new User;
    user.username = 'ALL';

    this.followupUsers.push(user)
  }

  ngOnInit() {
    let currentUser:string = localStorage.getItem('currentUser')
    let seletedDateOption:string = localStorage.getItem('seletedDateOption')
    let selectedRegionNumber:string = localStorage.getItem('selectedRegionNumber')
    let selectedOwner:string = localStorage.getItem('selectedOwner')
    let ownerRegionCriteria:string = localStorage.getItem('ownerRegionCriteria')

    if (currentUser == null || currentUser == 'undefined') {
      this.router.navigate(['/login']);
    } else {
      this.currentUser = JSON.parse(currentUser);
      if (!this.regions) {
        this.regions = [
        {
          regionName: 'AR1',
          regionNumber: "AR1"
        },
        {
          regionName: 'AR2',
          regionNumber: "AR2"
        },
        {
          regionName: 'AR3',
          regionNumber: "AR3"
        },
        {
          regionName: 'AR4',
          regionNumber: "AR4"
        },
        {
          regionName: 'AR5',
          regionNumber: "AR5"
        },
        {
          regionName: 'AR6',
          regionNumber: "AR6"
        },
        {
          regionName: 'AR7',
          regionNumber: "AR7"
        },
        {
          regionName: 'AR8',
          regionNumber: "AR8"
        },
        {
          regionName: 'SPR',
          regionNumber: "SPR"
        },
        {
          regionName: 'CR1',
          regionNumber: "CR1"
        },
        {
          regionName: 'INC',
          regionNumber: "INC"
        },
        {
          regionName: 'All Regions',
          regionNumber: "ALL"
        }
      ]
      }
      if (selectedRegionNumber != null) {
        this.selectedRegionNumber = selectedRegionNumber;
      }
      if (!this.selectedRegionNumber) {
        this.selectedRegionNumber = this.currentUser.regionNumber;
      }
      if (seletedDateOption != null) {
        this.selectedDayOption = seletedDateOption;
      }
      if (selectedOwner != null) {
        this.selectedOwner = selectedOwner;
      }
      if (ownerRegionCriteria != null) {
        this.ownerRegionCriteria = ownerRegionCriteria;
      }
      this.getFollowupUsers();
      this.getFollowups()
    }
  }
  updateSelectedRegion():void {
    this.setOwnerRegionCriteria('region');
    localStorage.setItem('selectedRegionNumber',this.selectedRegionNumber);
    this.getFollowups()
  }
  updateSelectedDayOption():void {
    localStorage.setItem('seletedDateOption',this.selectedDayOption);
    this.getFollowups()
  }
  updateSelectedOwner():void {
    this.setOwnerRegionCriteria('owner');
    localStorage.setItem('selectedOwner',this.selectedOwner);
    this.getFollowups()
  }
  setOwnerRegionCriteria(criteria:string):void {
    this.ownerRegionCriteria = criteria;
    localStorage.setItem('ownerRegionCriteria',this.ownerRegionCriteria);
    if (criteria == 'owner') {
      this.selectedRegionNumber = 'ALL'
      localStorage.setItem('selectedRegionNumber',this.selectedRegionNumber);
    }
    if (criteria == 'region') {
      this.selectedOwner = 'ALL'
      localStorage.setItem('selectedOwner',this.selectedOwner);
    }
  }
  getFollowupUsers() {
    if (this.followupUsers.length == 1) {
      this.userService.getFollowupUsers().then(followupUsers => this.addFollowupUsers(followupUsers))
    }
  }
  addFollowupUsers(followupUsers:User[]):void {
    for (let followupUser of followupUsers) {
      this.followupUsers.push(followupUser);
    }
  }
  getFollowups():void {
    if (this.ownerRegionCriteria == 'region') {
      this.followupService.getActiveFollowupsByRegionDay(this.selectedRegionNumber, this.selectedDayOption).then( (followups) => this.followups = followups );
    }
    if (this.ownerRegionCriteria == 'owner') {
      this.followupService.getActiveFollowupsByOwnerDay(this.selectedOwner, this.selectedDayOption).then( (followups) => this.followups = followups );
    }
  }

}
