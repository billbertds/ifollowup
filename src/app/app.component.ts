import {Component, NgModule, OnInit} from '@angular/core';
import {DashboardComponent} from './dashboard/dashboard.component'
import {CustomersearchComponent} from './customersearch/customersearch.component'
import {FollowupComponent} from './followup/followup.component'
import {LoginComponent} from "./login/login.component";

import {User} from "./user";
import {UserService} from "./user.service";

@NgModule ({
  declarations: [
    DashboardComponent,
    CustomersearchComponent,
    FollowupComponent,
    LoginComponent
  ]
})
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})

export class AppComponent  implements OnInit {
  currentUser:User;
  userAuthenticated:Boolean;
  title = 'AR Follow-up App';

  constructor(private userService: UserService) {
    let currentUser:string = localStorage.getItem('currentUser')

    if (!(currentUser == null || currentUser == 'undefined')) {
      this.currentUser = JSON.parse(currentUser);
      this.userAuthenticated = true;
    } else {
      this.currentUser = new User();
      this.userAuthenticated = false;
    }
  }

  isAuthenticated():Boolean {
    return this.userAuthenticated;
  }
  ngOnInit() {
  }

}
