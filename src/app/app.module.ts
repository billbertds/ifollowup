import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component'
import {CustomersearchComponent} from './customersearch/customersearch.component'
import {FollowupComponent} from './followup/followup.component'

import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';

import { DatepickerModule } from 'angular2-material-datepicker'

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CustomersearchComponent,
    FollowupComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    DatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
