/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FollowupService } from './followup.service';

describe('Service: Followup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FollowupService]
    });
  });

  it('should ...', inject([FollowupService], (service: FollowupService) => {
    expect(service).toBeTruthy();
  }));
});
