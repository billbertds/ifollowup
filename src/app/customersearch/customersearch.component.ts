import { Component, OnInit } from '@angular/core';
import {FollowupService} from "../followup.service";
import {Router} from "@angular/router";
import {isUndefined} from "util";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-customersearch',
  templateUrl: './customersearch.component.html',
  styleUrls: ['./customersearch.component.css'],
  providers: [FollowupService]
})
export class CustomersearchComponent implements OnInit {
  customerName;
  customerNumber;
  searchResults = [];

  constructor(private followupService:FollowupService,
              private router: Router) { }

  ngOnInit() {
    let currentUser:string = localStorage.getItem('currentUser')

    if (currentUser == null || currentUser == 'undefined') {
      this.router.navigate(['/login']);
    }
  }
  lookupCustomer():void {
    let customerName = this.customerName;
    let customerNumber:String = this.customerNumber;

    if (customerName == '' || customerName == null) {
      customerName = 'EMPTY';
    }
    if (customerNumber == '' || customerNumber == null) {
      customerNumber = 'EMPTY';
    }
    if (customerName == 'EMPTY' && customerNumber == 'EMPTY') {
      // todo create a nice message to the user
      return;
    }

    this.followupService.getCustomersByNameOrNumber(customerName,customerNumber).then(searchResults => this.searchResults = searchResults);
  }

}
