/**
 * Created by wchan on 11/11/2016.
 */
export class Customer {

  customerNumber: string
  customerName: string
  regionNumber: string

  copyObject(customer:Customer, object) {

    for (let key in object)
    {
      if (object.hasOwnProperty(key))
      {
        customer[key] = object[key];
      }
    }
  }
  constructor() {
  }
}
