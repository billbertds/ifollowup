/**
 * Created by wchan on 12/20/2016.
 */

export class PromisesPromises {
  currentDate: Date
  contactPhone: string
  contactName: string
  what: string
  when: string
  followupDesc: string
  followupDate: Date
  repNotified: string
  constructor(object) {
    this.currentDate = new Date;
    this.followupDate = new Date;
  }
}
