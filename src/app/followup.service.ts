import { Injectable } from '@angular/core';
import {Followup, StringNull, DateNull} from "./followup";
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {Customer} from "./customer";

@Injectable()
export class FollowupService {

  private lastFollowupId: number = 0;
  private followupsByRegion;
  private customersByNameOrNumber;
  private activeFollowupsByRegion:Followup[];
  private activeFollowupsByOwner:Followup[];
  private followupsByCustomer:Followup[] = [];
  private lastUpdatedFollowup:Followup;
  private customerByNumber;
  private followupBaseUrl: string = 'http://localhost:8081'
  private followupsUrl: string = this.followupBaseUrl + '/followups'
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  getCustomersByNameOrNumber(customerName:String, customerNumber:String):Promise<Object[]> {
    let customerByNameOrNumberUrl: string = this.followupBaseUrl + '/customersByNameOrNumber/'+customerName.toString()+'/'+ customerNumber

    return this.http.get(customerByNameOrNumberUrl)
      .toPromise()
      .then(response => this.customersByNameOrNumber = response.json() as Object[])
      .catch(this.handleError);
  }
  getCustomerByNumber(customerNumber:string):Promise<Customer> {
    let customerByNumberUrl: string = this.followupBaseUrl + '/customer/'+customerNumber.toString()

    return this.http.get(customerByNumberUrl)
      .toPromise()
      .then(response => this.customerByNumber = response.json() as Customer)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
  getActiveFollowupsByRegionDay(regionNumber:string, dayOption:string):Promise<Followup> {

    var followupsByRegionUrl: string = this.followupBaseUrl + '/followupsByRegion/' + regionNumber + '/' + dayOption

    return this.http.get(followupsByRegionUrl)
      .toPromise()
      .then(response => this.activeFollowupsByRegion = this.getActiveFollowupsByRegionResponse(response.json()))
      .catch(this.handleError);
  }
  getActiveFollowupsByOwnerDay(owner:string, dayOption:string):Promise<Followup> {

    var followupsByOwnerUrl: string = this.followupBaseUrl + '/followupsByOwner/' + owner + '/' + dayOption

    return this.http.get(followupsByOwnerUrl)
      .toPromise()
      .then(response => this.activeFollowupsByOwner = this.getActiveFollowupsByOwnerResponse(response.json()))
      .catch(this.handleError);
  }
  getActiveFollowupsByOwnerResponse(objects): Followup[] {
    let returnFollowups:Followup[] = []

    for (let object of objects) {
      returnFollowups.push( new Followup(object));
    }
    return returnFollowups
  }
  getActiveFollowupsByRegion(regionNumber:string):Promise<Followup> {

    var followupsByRegionUrl: string = this.followupBaseUrl + '/followupsByRegion/' + regionNumber

    return this.http.get(followupsByRegionUrl)
      .toPromise()
      .then(response => this.activeFollowupsByRegion = this.getActiveFollowupsByRegionResponse(response.json()))
      .catch(this.handleError);
  }
  getActiveFollowupsByRegionResponse(objects): Followup[] {
    let returnFollowups:Followup[] = []

    for (let object of objects) {
      returnFollowups.push( new Followup(object));
    }
    return returnFollowups
  }
  getActiveFollowupsByCustomerNumber(customerNumber:string):Promise<Followup[]> {
    let customerByNumberUrl: string = this.followupBaseUrl + '/followupsByCustomer/'+customerNumber+'/A'

    return this.http.get(customerByNumberUrl)
      .toPromise()
      .then( response => this.followupsByCustomer = this.getActiveFollowupsByCustomerNumberResponse(response.json()) )
      .catch(this.handleError);
  }
  getActiveFollowupsByCustomerNumberResponse(objects): Followup[] {
    let returnFollowups:Followup[] = []

    for (let object of objects) {
      returnFollowups.push( new Followup(object));
    }
    return returnFollowups
  }
  getCompletedFollowupsByCustomerNumber(customerNumber:string):Promise<Followup[]> {
    let customerByNumberUrl: string = this.followupBaseUrl + '/followupsByCustomer/'+customerNumber+'/C'

    return this.http.get(customerByNumberUrl)
      .toPromise()
      .then( response => this.getCompletedFollowupsByCustomerNumberResponse(response.json()) )
      .catch(this.handleError);
  }
  getCompletedFollowupsByCustomerNumberResponse(objects): Followup[] {
    let returnString:string = ""

    for (let object of objects) {
      let newFollowup:Followup = new Followup(object);
      this.followupsByCustomer.push( newFollowup);
    }
    return this.followupsByCustomer
  }
  addFollowup(followup:Followup): Promise<Followup> {
    let followupAddUrl: string = this.followupBaseUrl + '/followup'

    // set initial default
    followup.createdFollowUpDate = followup.followupDate;

    // send to service to save
    return this.http.post(followupAddUrl,JSON.stringify(followup))
      .toPromise()
      .then(response => this.addFollowupResponse(response.json() as Followup) )
      .catch(this.handleError);
  }
  addFollowupResponse(followup):Followup {
    let updateFollowup:Followup = this.getFollowupById(0);
    updateFollowup.followupId = followup.followupId;
    updateFollowup.lastModifiedDate = new Date(followup.lastModifiedDate);

    return updateFollowup;
  }
  getFollowupById(id:number): Followup {

    return this.followupsByCustomer
      .filter((followup) => followup.followupId == id)
      .pop();
  }

  followupCompleted(followupId:number, username:string): Promise<Followup> {
    let updatedFollowup:Followup;
    let foundFollowup:Followup;
    let followupAddUrl: string = this.followupBaseUrl + '/followup/' + followupId.toString()

    foundFollowup = this.getFollowupById(followupId);

    if (foundFollowup) {
      let today = new Date()
      let dd = today.getDate();
      let mm = today.getMonth()+1;
      let yyyy = today.getFullYear();

      // Complete Process
      if (!foundFollowup.completedDate) {
        foundFollowup.completedDate = new DateNull()
      }
      if (!foundFollowup.completedBy) {
        foundFollowup.completedBy = new StringNull()
      }
      foundFollowup.completedDate.String = today;
      foundFollowup.completedDate.Valid = true;
      foundFollowup.completedBy.String = username;
      foundFollowup.completedBy.Valid = true;
      foundFollowup.lastModifiedDate = today;
      foundFollowup.lastModifiedBy = username;

      // Remove followup from active array
      for(let i = 0; i < this.followupsByCustomer.length; i++) {
        if (this.followupsByCustomer[i].followupId == followupId){
          this.followupsByCustomer[i].completedDate = foundFollowup.completedDate
          this.followupsByCustomer[i].completedBy = foundFollowup.completedBy
          this.followupsByCustomer[i].lastModifiedDate = foundFollowup.lastModifiedDate
          this.followupsByCustomer[i].lastModifiedBy = foundFollowup.lastModifiedBy
//          this.activeFollowupsByCustomer.splice(i,1);
        }
      }
      return this.http.post(followupAddUrl,JSON.stringify(foundFollowup))
        .toPromise()
        .then(response => this.lastUpdatedFollowup = response.json() as Followup)
        .catch(this.handleError);
    } else {
      return Promise.resolve(foundFollowup);
    }
  }
  followupReopen(followupId:number, username:string): Promise<Followup> {
    let updatedFollowup:Followup;
    let foundFollowup:Followup;
    let followupAddUrl: string = this.followupBaseUrl + '/followup/' + followupId.toString()

    foundFollowup = this.getFollowupById(followupId);

    if (foundFollowup) {
      let today = new Date()
      let dd = today.getDate();
      let mm = today.getMonth()+1;
      let yyyy = today.getFullYear();

      // Complete Process
      if (!foundFollowup.completedDate) {
        foundFollowup.completedDate = new DateNull()
      }
      if (!foundFollowup.completedBy) {
        foundFollowup.completedBy = new StringNull()
      }
      foundFollowup.completedDate.Valid = false;
      foundFollowup.completedBy.String = "";
      foundFollowup.completedBy.Valid = false;
      foundFollowup.lastModifiedDate = today;
      foundFollowup.lastModifiedBy = username;

      // Remove followup from active array
      for(let i = 0; i < this.followupsByCustomer.length; i++) {
        if (this.followupsByCustomer[i].followupId == followupId){
          this.followupsByCustomer[i].completedDate = foundFollowup.completedDate
          this.followupsByCustomer[i].completedBy.String = foundFollowup.completedBy.String
          this.followupsByCustomer[i].lastModifiedDate = foundFollowup.lastModifiedDate
          this.followupsByCustomer[i].lastModifiedBy = foundFollowup.lastModifiedBy
//          this.activeFollowupsByCustomer.splice(i,1);
        }
      }
      return this.http.post(followupAddUrl,JSON.stringify(foundFollowup))
        .toPromise()
        .then(response => this.lastUpdatedFollowup = response.json() as Followup)
        .catch(this.handleError);
    } else {
      return Promise.resolve(foundFollowup);
    }
  }
  updateFollowupById(followup:Followup): Promise<Followup> {
    let followupAddUrl: string = this.followupBaseUrl + '/followup/' + followup.followupId.toString()

    return this.http.post(followupAddUrl,JSON.stringify(followup))
      .toPromise()
      .then( response => this.updateFollowupResponse(response.json() as Followup) )
      .catch(this.handleError);
  }
  updateFollowupResponse(followup:Followup):Followup {
    let updateFollowup:Followup = this.getFollowupById(followup.followupId);

    updateFollowup.lastModifiedDate = new Date(followup.lastModifiedDate);

    return updateFollowup;
  }
}
