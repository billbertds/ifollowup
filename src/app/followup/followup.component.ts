import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import {FollowupService} from "../followup.service";
import {Followup, DateNull, StringNull} from "../followup";
import {User} from "../user";
import {Customer} from "../customer";
import {PromisesPromises} from "../promisespromises";
import {UserService} from "../user.service";

@Component({
  selector: `'app-followup'`,
  templateUrl: './followup.component.html',
  styleUrls: ['./followup.component.css'],
  providers: [FollowupService,UserService]
})
export class FollowupComponent implements OnInit {
  activeFollowups;
  newFollowupSaveEnabled:Boolean = false
  customer = new Customer();
  currentCustomerNumber:string;
  currentUser:User;
  lastCompletedFollowup:Followup;
  lastReopenFollowup:Followup;
  includeCompletedCheckbox:Boolean = false;
  promisesPromises:PromisesPromises = new PromisesPromises({});
  followupTypes = [
    {
      type: 'AR',
      desc: 'AR'
    },
    {
      type: 'Recon',
      desc: 'Recon'
    },
    {
      type: 'Other',
      desc: 'Other'
    }
  ]
  followupParties = [
    {
      party: 'Internal',
      desc: 'Internal'
    },
    {
      party: 'Customer',
      desc: 'Customer'
    }
  ]
  followupUsers : User[] = [];

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private followupService: FollowupService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.params.forEach((params: Params) => {
      let id = params['id'];
      this.currentCustomerNumber = id;
    })
    this.getClient();
    this.getActiveFollowups();
    this.getFollowupUsers();
    this.includeCompletedCheckbox = false;
  }
  getClient() {
    this.followupService.getCustomerByNumber(this.currentCustomerNumber).then(customer => this.customer = customer)
  }
  getActiveFollowups() {
    this.followupService.getActiveFollowupsByCustomerNumber(this.currentCustomerNumber).then(activeFollowups => this.activeFollowups = activeFollowups)
  }
  getFollowupUsers() {
    if (this.followupUsers.length == 0) {
      this.userService.getFollowupUsers().then(followupUsers => this.followupUsers = followupUsers)
    }
  }
  addFollowup():void {
    // Only allow one new record.
    if (this.activeFollowups.filter( item => item.followupId == 0).length > 0) {
      return;
    }
    let today = new Date()

    let obj = {
      followupId: 0,
      createdBy: this.currentUser.username,
      createdOn: today.toJSON(),
      lastModifiedBy: this.currentUser.username,
      lastModifiedDate: today.toJSON(),
      createdFollowUpDate: today.toJSON(),
      customerNumber: this.customer.customerNumber,
      customerName: this.customer.customerName,
      regionNumber: this.customer.regionNumber,
      followupParty: 'Customer',
      followupOwner: this.currentUser.username,
      completedBy : new StringNull(),
      completedDate : new DateNull()
    };
//    obj.completedBy = new StringNull;

    let newFollowup = new Followup(obj);

    // add followup at the beginning
    this.activeFollowups.splice(0,0,newFollowup);
    this.activeFollowups[0].completedBy.String = "";
    this.activeFollowups[0].completedBy.Valid = false;
    this.activeFollowups[0].completedDate.String = new Date();
    this.activeFollowups[0].completedDate.Valid = false;


    // Allow save of the new followup
    this.newFollowupSaveEnabled = true;
  }
  updateFollowup(followupId:number) {
    let followup:Followup;
    let index:number;

    for(let i = 0; i < this.activeFollowups.length; i++) {
      if (this.activeFollowups[i].followupId == followupId){
        followup = this.activeFollowups[i];
        index = i;
      }
    }

    if (followupId == 0) {
      // validate data
      if (!followup.followupDate || (!followup.followupDesc) || (!followup.followupType)) {
        // todo notify user that something is missing
        return;
      }
      // check for double clicks to only save once
      if (this.newFollowupSaveEnabled) {
        this.newFollowupSaveEnabled = false;

        this.followupService.addFollowup(followup);
      }
      return;
    }

    followup.lastModifiedBy = this.currentUser.username;
    this.followupService.updateFollowupById(followup);
    this.displayPromises(followup);
  }
  displayPromises(followup:Followup) {
    this.promisesPromises.contactPhone = followup.contactPhone
    this.promisesPromises.contactName = followup.contact
    this.promisesPromises.what = followup.followupDesc
    this.promisesPromises.when = ""
    this.promisesPromises.followupDesc = ""
    this.promisesPromises.followupDate = new Date(followup.followupDate.toISOString())
    this.promisesPromises.repNotified = ""
  }
  completeFollowup(followupId) {
    if (followupId > 0) {
      this.followupService.followupCompleted(followupId, this.currentUser.username).then(lastCompletedFollowup => this.lastCompletedFollowup = lastCompletedFollowup)
    }
  }
  reopenFollowup(followupId) {
    if (followupId > 0) {
      this.followupService.followupReopen(followupId, this.currentUser.username).then(lastReopenFollowup => this.lastReopenFollowup = lastReopenFollowup)
    }
  }
  goBack(): void {
    this.location.back()
  }
  toggleIncludeCompleted(): void {
    this.includeCompletedCheckbox = !this.includeCompletedCheckbox

    if (this.includeCompletedCheckbox) {
      this.followupService.getCompletedFollowupsByCustomerNumber(this.currentCustomerNumber).then(activeFollowups => this.activeFollowups = activeFollowups)
    } else {
      for (let key = (this.activeFollowups.length - 1); key >= 0; key--) {
        if (this.activeFollowups[key].completedDate.Valid) {
          // remove the value
          this.activeFollowups.splice(key, 1);
        }
      }
    }
  }
}
