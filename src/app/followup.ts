export class StringNull {
  String: string
  Valid: Boolean
}
export class DateNull {
  String: Date
  Valid: Boolean
}

export class Followup {
  followupId: number
  customerNumber: string
  customerName: string
  regionNumber: string
  followupType: string
  followupDesc : string
  followupDate : Date
  followupParty : string
  followupOwner : string
  contact : string
  contactTitle : string
  contactPhone : string
  contactEmail : string
  createdBy : string
  createdOn : Date
  createdFollowUpDate : Date
  lastModifiedBy : string
  lastModifiedDate : Date
  completedDate : DateNull
  completedBy : StringNull;

  constructor(object) {
    let dateKeys: string[] = ["followupDate","createdOn","createdFollowUpDate","lastModifiedDate","completedDate"]

    for (let key in object) {
      if (object.hasOwnProperty(key))
      {
        if (dateKeys.filter(x => x == key).length > 0) {
          if (key == "completedDate") {
            this[key] = new DateNull();
            if (object[key].String == "") {
              this[key].String = new Date();
              this[key].Valid = false
            } else {
              this[key].String = new Date(object[key].String)
              this[key].Valid = true
            }
          } else if (key == "completedBy") {
            this[key] = new StringNull();
            this[key].String = object[key].String
            this[key].Valid = object[key].Valid
          } else {
            this[key] = new Date(object[key])
          }
        } else {
          this[key] = object[key];
        }
      }
    }
  }
}
