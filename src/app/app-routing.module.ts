import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomersearchComponent } from './customersearch/customersearch.component';
import { FollowupComponent } from './followup/followup.component';
import {LoginComponent} from "./login/login.component";


const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: DashboardComponent
  },
  {
    path: 'followup/:id',
    component: FollowupComponent
  },
  {
    path: 'customersearch',
    component: CustomersearchComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
