import { Component, OnInit } from '@angular/core';
import {User} from "../user";
import { UserService } from "../user.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  user:User;
  username;
  userPassword;
  loginResult = 'Please enter username and password.';
  constructor(private userService : UserService,
              private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }
  loginAttempt(): String {
    this.loginResult = 'Attempting to Authenticate.'
    this.userService.authenticateUser(this.username).then(user => this.loginAttemptResponse(user)).catch(err => this.networkError(err))
    return ''
  }
  networkError(err:any) {
    this.loginResult = 'Network Error contact Rose in Reporting (Backend service cannot be reached).'
  }
  loginAttemptResponse(user:User) {

    // store user details and jwt token in local storage to keep user logged in between page refreshes
    if (this.username == user.username && this.userPassword == user.password) {

      user.password = "";
      localStorage.setItem('currentUser', JSON.stringify(user));

      this.router.navigate(['/home']);
    } else {
      localStorage.removeItem('currentUser');
      this.loginResult = 'Username or Password is not valid please try again.'
    }
  }
}
