import { Injectable } from '@angular/core';
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {User} from "./user";

@Injectable()
export class UserService {
  authenticatedUser:User;
  authenticated:Boolean = false;
  userBaseUrl:string = 'http://localhost:8081'

  constructor(private http: Http) { }

  authenticateUser(username:string): Promise<User> {

    var followupsByRegionUrl: string = this.userBaseUrl + '/userAuthentication/' + username

    return this.http.get(followupsByRegionUrl)
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }
  getFollowupUsers(): Promise<User[]> {
    var followupUsersUrl: string = this.userBaseUrl + '/usersByGroup/Followup'

    // todo Store result in array and return result instead of round trip to server
    return this.http.get(followupUsersUrl)
      .toPromise()
      .then(response => response.json() as User[])
      .catch(this.handleError);
  }
  logoffUser():void {
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
